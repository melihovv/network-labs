import networkx as nx
import pylab
from random import randint


def close_figures(graphs):
    pylab.ion()
    for title in graphs:
        pylab.close(title)


def draw_graphs(graphs):
    for title in graphs:
        g = graphs[title]
        pos = nx.spring_layout(g)

        pylab.figure(title)
        nx.draw(g, pos)

        edge_labels = dict([((u, v,), d['weight'])
                            for u, v, d in g.edges(data=True)])
        nx.draw_networkx_edge_labels(g, pos, edge_labels=edge_labels)

        nx.draw_networkx_nodes(g, pos, node_size=700)
        nx.draw_networkx_labels(g, pos, font_size=20,
                                font_family='sans-serif')

    pylab.show()


def generate_graph(vertex_amount, edge_amount):
    g = nx.complete_graph(vertex_amount)
    g = nx.minimum_spanning_tree(g)

    correct_number_of_edges(g, edge_amount)
    generate_weights(g)

    print(g.number_of_nodes())
    print(g.number_of_edges())

    return g


def generate_weights(graph):
    for u, v, d in graph.edges(data=True):
        d['weight'] = randint(1, 10)


def correct_number_of_edges(graph, edge_amount):
    edges = graph.number_of_edges()
    diff = abs(edge_amount - edges)

    if edges < edge_amount:
        while diff != 0:
            for u in graph.nodes():
                for v in graph.nodes():
                    if u != v and v not in graph.neighbors(u):
                        if diff != 0:
                            graph.add_edge(u, v)
                            diff -= 1
                            break
                        else:
                            return
