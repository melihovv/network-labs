# -*- coding: utf-8 -*-

import sys
from PyQt4 import QtCore, QtGui
from PyQt4.uic import loadUiType
from graph_helpers import *
import networkx as nx

app = QtGui.QApplication(sys.argv)
app.setApplicationName('mst')
form_class, base_class = loadUiType('mainwindow.ui')


class MainWindow(QtGui.QMainWindow, form_class):
    def __init__(self):
        super(MainWindow, self).__init__()

        self.ui = form_class()
        self.ui.setupUi(self)

        self.make_connection()

        self.run_counter = 0

    def make_connection(self):
        self.ui.btn_run.clicked.connect(self.on_btn_run_clicked)
        self.ui.vertex_amount.valueChanged.connect(
            self.on_vertex_amount_changed
        )

    @QtCore.pyqtSlot()
    def on_btn_run_clicked(self):
        self.run_counter += 1
        if self.run_counter == 2:
            self.run_counter = 0
            return

        vertex_amount = self.ui.vertex_amount.value()
        edge_amount = self.ui.edge_amount.value()
        g = generate_graph(vertex_amount, edge_amount)
        mst = nx.minimum_spanning_tree(g)
        close_figures({'Graph': g, 'MST': mst})
        draw_graphs({'Graph': g, 'MST': mst})

    # количество ребер ~ кратность связей
    # max: n * (n - 1) / 2 * (кратность связей)

    # количество ребер ~ количество узлов
    # min: n - 1
    # max: n * (n - 1) / 2 * (кратность связей)

    @QtCore.pyqtSlot(int)
    def on_vertex_amount_changed(self, val):
        self.ui.edge_amount.setMinimum(val - 1)
        self.ui.edge_amount.setMaximum(val * (val - 1) / 2)


if __name__ == '__main__':
    window = MainWindow()
    window.setWindowTitle('mst')
    window.show()
    sys.exit(app.exec_())
